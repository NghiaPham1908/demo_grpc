//
//  main.m
//  demo
//
//  Created by PVNghia on 9/20/21.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#import <GRpc>

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
