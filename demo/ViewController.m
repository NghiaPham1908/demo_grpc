//
//  ViewController.m
//  demo
//
//  Created by PVNghia on 9/20/21.
//

#import "ViewController.h"
#import <GRPCClient/GRPCCall+ChannelArg.h>
#import <GRPCClient/GRPCTransport.h>
#import <grpcclient/GRPCCall+Tests.h>
#if COCOAPODS
    #import <demoPodspec/Demo.pbrpc.h>
//#else
#endif
static NSString* const hostAddress = @"localhost:8080";
@interface ViewController ()

@end
 
@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    //PetService *client = [[PetService alloc] initWithHost:hostAddress];
    
    //PetList *list = [PetList message];
    //list.petsArray
    
   
    GRPCMutableCallOptions *options = [[GRPCMutableCallOptions alloc] init];
    options.transport = GRPCDefaultTransportImplList.core_insecure;
    GRPCCallOptions *op = [options copy];
    PetService *client = [[PetService alloc] initWithHost:hostAddress];
    
//    GRPCUnaryResponseHandler *handler = [[GRPCUnaryResponseHandler alloc] initWithResponseHandler:^(PetList *list, NSError *error) {
//        if(error){
//            NSLog(@"%@",error);
//        }else{
//            NSLog(@"%@",list);
//        }
//    } responseDispatchQueue:nil];
//    Empty *emty = [Empty alloc];
//    [[client listWithMessage:emty responseHandler:handler callOptions:nil] start];
    Empty *emty = [Empty alloc];
    [client listWithRequest:emty handler:^(PetList * _Nullable response, NSError * _Nullable error) {
            if(error){
                NSLog(@"%@",error);
            }else{
                NSLog(@"%@",response);
            }
    }];
    
    // Do any additional setup after loading the view.
}


//@synthesize dispatchQueue;

@end
