//
//  SceneDelegate.h
//  demo
//
//  Created by PVNghia on 9/20/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

