#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "Demo.pbobjc.h"
#import "Demo.pbrpc.h"

FOUNDATION_EXPORT double demoPodspecVersionNumber;
FOUNDATION_EXPORT const unsigned char demoPodspecVersionString[];

