// Code generated by gRPC proto compiler.  DO NOT EDIT!
// source: demo.proto

#if !defined(GPB_GRPC_PROTOCOL_ONLY) || !GPB_GRPC_PROTOCOL_ONLY
#import "Demo.pbrpc.h"
#import "Demo.pbobjc.h"
#import <ProtoRPC/ProtoRPCLegacy.h>
#import <RxLibrary/GRXWriter+Immediate.h>


@implementation PetService

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"

// Designated initializer
- (instancetype)initWithHost:(NSString *)host callOptions:(GRPCCallOptions *_Nullable)callOptions {
  return [super initWithHost:host
                 packageName:@""
                 serviceName:@"PetService"
                 callOptions:callOptions];
}

- (instancetype)initWithHost:(NSString *)host {
  return [super initWithHost:host
                 packageName:@""
                 serviceName:@"PetService"];
}

#pragma clang diagnostic pop

// Override superclass initializer to disallow different package and service names.
- (instancetype)initWithHost:(NSString *)host
                 packageName:(NSString *)packageName
                 serviceName:(NSString *)serviceName {
  return [self initWithHost:host];
}

- (instancetype)initWithHost:(NSString *)host
                 packageName:(NSString *)packageName
                 serviceName:(NSString *)serviceName
                 callOptions:(GRPCCallOptions *)callOptions {
  return [self initWithHost:host callOptions:callOptions];
}

#pragma mark - Class Methods

+ (instancetype)serviceWithHost:(NSString *)host {
  return [[self alloc] initWithHost:host];
}

+ (instancetype)serviceWithHost:(NSString *)host callOptions:(GRPCCallOptions *_Nullable)callOptions {
  return [[self alloc] initWithHost:host callOptions:callOptions];
}

#pragma mark - Method Implementations

#pragma mark List(Empty) returns (PetList)

- (void)listWithRequest:(Empty *)request handler:(void(^)(PetList *_Nullable response, NSError *_Nullable error))handler{
  [[self RPCToListWithRequest:request handler:handler] start];
}
// Returns a not-yet-started RPC object.
- (GRPCProtoCall *)RPCToListWithRequest:(Empty *)request handler:(void(^)(PetList *_Nullable response, NSError *_Nullable error))handler{
  return [self RPCToMethod:@"List"
            requestsWriter:[GRXWriter writerWithValue:request]
             responseClass:[PetList class]
        responsesWriteable:[GRXWriteable writeableWithSingleHandler:handler]];
}
- (GRPCUnaryProtoCall *)listWithMessage:(Empty *)message responseHandler:(id<GRPCProtoResponseHandler>)handler callOptions:(GRPCCallOptions *_Nullable)callOptions {
  return [self RPCToMethod:@"List"
                   message:message
           responseHandler:handler
               callOptions:callOptions
             responseClass:[PetList class]];
}

#pragma mark Insert(Pet) returns (Pet)

- (void)insertWithRequest:(Pet *)request handler:(void(^)(Pet *_Nullable response, NSError *_Nullable error))handler{
  [[self RPCToInsertWithRequest:request handler:handler] start];
}
// Returns a not-yet-started RPC object.
- (GRPCProtoCall *)RPCToInsertWithRequest:(Pet *)request handler:(void(^)(Pet *_Nullable response, NSError *_Nullable error))handler{
  return [self RPCToMethod:@"Insert"
            requestsWriter:[GRXWriter writerWithValue:request]
             responseClass:[Pet class]
        responsesWriteable:[GRXWriteable writeableWithSingleHandler:handler]];
}
- (GRPCUnaryProtoCall *)insertWithMessage:(Pet *)message responseHandler:(id<GRPCProtoResponseHandler>)handler callOptions:(GRPCCallOptions *_Nullable)callOptions {
  return [self RPCToMethod:@"Insert"
                   message:message
           responseHandler:handler
               callOptions:callOptions
             responseClass:[Pet class]];
}

#pragma mark Delete(PetRequestId) returns (Pet)

- (void)deleteWithRequest:(PetRequestId *)request handler:(void(^)(Pet *_Nullable response, NSError *_Nullable error))handler{
  [[self RPCToDeleteWithRequest:request handler:handler] start];
}
// Returns a not-yet-started RPC object.
- (GRPCProtoCall *)RPCToDeleteWithRequest:(PetRequestId *)request handler:(void(^)(Pet *_Nullable response, NSError *_Nullable error))handler{
  return [self RPCToMethod:@"Delete"
            requestsWriter:[GRXWriter writerWithValue:request]
             responseClass:[Pet class]
        responsesWriteable:[GRXWriteable writeableWithSingleHandler:handler]];
}
- (GRPCUnaryProtoCall *)deleteWithMessage:(PetRequestId *)message responseHandler:(id<GRPCProtoResponseHandler>)handler callOptions:(GRPCCallOptions *_Nullable)callOptions {
  return [self RPCToMethod:@"Delete"
                   message:message
           responseHandler:handler
               callOptions:callOptions
             responseClass:[Pet class]];
}

@end
#endif
